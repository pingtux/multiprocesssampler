import numpy as np
from deep500.lv2.sampler import Sampler, SamplerEvent, Dataset
import ctypes
from typing import Any, Callable, Dict, List, Optional

from multiprocessing import Process, Array, BoundedSemaphore


class MultiProcessSampler(Sampler):
    """ Simple data loader that prefetches data with a specified number of additional processes """

    def __init__(
            self,
            dataset: Dataset,
            batch_size: int,
            seed: int = None,
            drop_last_batch: bool = True,
            events: List[SamplerEvent] = None,
            transformations: Callable[[Dict[str, Any]], Dict[str, Any]] = None,
            sub_processes: int = 2,
            maximum_buffer_length: int = 32768
    ):
        self.batch_idx = 0
        self.batch_offset = 0
        self.pid_counter = 0
        self.sub_processes = sub_processes
        super().__init__(dataset, batch_size, seed, drop_last_batch,
                         events, transformations)
        self.sampler_processes = []
        # Number of batches that are prefetched
        self.max_batches = int(maximum_buffer_length / batch_size)
        assert (self.max_batches > 0)
        data_shape = dataset[0][dataset.input_node].shape
        data_type = dataset[0][dataset.input_node].dtype
        if data_type == np.float32:
            ctype = ctypes.c_float
        elif data_type == np.float64:
            ctype = ctypes.c_double
        elif data_type == np.int:
            ctype = ctypes.c_int
        else:
            raise NotImplementedError("Datatype is not supported")
        batch_elements = 1
        for i in data_shape:
            batch_elements *= i
        self.data_arrays = []
        self.semaphores = []
        for _ in range(sub_processes):
            data_array_base = Array(ctype, (self.max_batches // sub_processes) * batch_size * batch_elements,
                                    lock=False)
            data_array = np.ctypeslib.as_array(data_array_base)
            data_array = data_array.reshape((self.max_batches // sub_processes, batch_size) + data_shape)
            self.data_arrays.append(data_array)
            self.semaphores.append(BoundedSemaphore(value=self.max_batches // sub_processes - 2))
        self.label_arrays = [None for _ in range(sub_processes)]
        if dataset.label_node is not None:
            label_data_type = dataset[0][dataset.label_node].dtype
            if label_data_type == np.float32:
                ctype = ctypes.c_float
            elif label_data_type == np.float64:
                ctype = ctypes.c_double
            elif label_data_type == np.int:
                ctype = ctypes.c_int
            else:
                raise NotImplementedError("Datatype is not supported")
            for i in range(sub_processes):
                label_array_base = Array(ctype, (self.max_batches // sub_processes) * batch_size, lock=False)
                label_array = np.ctypeslib.as_array(label_array_base)
                label_array = label_array.reshape(self.max_batches // sub_processes, batch_size)
                self.label_arrays[i] = label_array
        for i in range(sub_processes):
            sampler_process = Process(target=_sample,
                                      args=(self.dataset,
                                            self.batch_size,
                                            self.data_arrays[i],
                                            self.label_arrays[i],
                                            super()._transform,
                                            i,
                                            sub_processes,
                                            self.drop_last_batch,
                                            self.random_state,
                                            self.semaphores[i]),
                                      daemon=True,
                                      name="sampler_process_{}".format(i))
            sampler_process.start()
            self.sampler_processes.append(sampler_process)

    def __del__(self):
        for child in self.sampler_processes:
            child.terminate()

    def __next__(self):
        for event in self.events:
            event.before_sampling(self, self.batch_size)
        if (self.drop_last_batch and
                self.batch_idx + self.batch_size > len(self.dataset)):
            raise StopIteration
        if self.batch_idx >= len(self.dataset):
            raise StopIteration

        ready = False
        while not ready:
            try:
                self.semaphores[self.pid_counter].release()
                ready = True
            except ValueError:
                # Occurs when the semaphore exceeds it's initial value (i.e. the producer was too slow),
                # which should almost never happen in practice. We therefore busy wait in these cases
                pass
        batch = {self.dataset.input_node: self.data_arrays[self.pid_counter][self.batch_offset]}
        if self.dataset.label_node is not None:
            batch[self.dataset.label_node] = self.label_arrays[self.pid_counter][self.batch_offset]
        self.batch_idx += self.batch_size

        self.pid_counter = (self.pid_counter + 1) % self.sub_processes
        if self.pid_counter == 0:
            self.batch_offset = (self.batch_offset + 1) % (self.max_batches // self.sub_processes)

        for event in self.events:
            event.after_sampling(self, batch)
        return batch

    def reset(self):
        super().reset()
        self.batch_idx = 0


def _sample(dataset: Dataset,
            batch_size: int,
            data_array: np.ndarray,
            label_array: Optional[np.ndarray],
            transform: Callable[[Dict[str, Any]], Dict[str, Any]],
            pid: int,
            no_processes: int,
            drop_last_batch: bool,
            random_state: np.random.RandomState,
            semaphore: BoundedSemaphore):
    """
    A process is responsible for putting batches pid, pid + no_processes, pid + 2*no_processes, etc... on its array.
    The semaphore ensures that a process doesn't overwrite values that weren't consumed.
    """
    sample_pool = np.arange(len(dataset))
    iterations = int(len(dataset) / batch_size)
    local_batches = data_array.shape[0]
    batch_offset = 0
    if not drop_last_batch:
        iterations += 1
    while True:
        random_state.shuffle(sample_pool)
        for batch_no in range(pid, iterations, no_processes):
            batch = dataset[sample_pool[batch_no * batch_size:
                                        min((batch_no + 1) * batch_size, len(dataset))]]
            transform(batch)
            data_array[batch_offset] = batch[dataset.input_node]
            if dataset.label_node is not None:
                label_array[batch_offset] = batch[dataset.label_node]
            semaphore.acquire()
            batch_offset = (batch_offset + 1) % local_batches
